library(ggplot2)

jeco_graph  <- function(data, series_label, title, legend_name, sources, x_label, y_label,
                        add_jeco = TRUE, linetype = NULL, group = NULL, directory = NULL,
                        filename = NULL){
    lim <- rep(NA_real_, 2)
    if (min(data$value, na.rm = TRUE) > 0) {
      lim <- c(0, NA_real_)
    }
    my_graph <- ggplot(data, aes_string(x = "period", y = "value", group = group,
                                        colour = series_label, linetype = linetype)) +
        geom_line(size = 1.2) +
        scale_x_date(expand = c(0, 0))
    my_graph  <- basic_settings(my_graph, title, legend_name, sources,
                                x_label, y_label, add_jeco, directory, filename)
    return(my_graph)
}

jeco_bar_graph2  <- function(data, x, fill, title, legend_name, sources, x_label, y_label,
                             add_jeco = TRUE, directory = NULL, filename = NULL){
    my_graph  <- ggplot(data=data, aes_string(x=x, y="value", fill=fill)) +
        geom_bar(stat="identity", position=position_dodge())
    my_graph  <- basic_settings(my_graph, title, legend_name, sources,
                                x_label, y_label, add_jeco, directory, filename)
    return(my_graph)
}

basic_settings  <- function(my_graph, title, legend_name, sources,
                            x_label, y_label, add_jeco, directory, filename){
    my_graph  <- my_graph +
        scale_y_continuous(
            labels = function(x) {
                format(
                    x,
                    big.mark = " ", scientific = FALSE, decimal.mark = ",", trim = TRUE,
                    drop0trailing = TRUE
                )
            }
        ) +
        scale_color_brewer(palette = "Dark2") +
        guides(fill = guide_legend(title = legend_name)) +
        labs(title = title) +
        xlab(x_label) +
        ylab(y_label) +
        annotate(
            geom = "text",
            label = paste0("Source: ", paste0(sources, collapse = ", "), " via DBnomics"),
            x = structure(Inf, class = "Date"), y = -Inf,
            hjust = 1.05, vjust = -0.5, col = "gray30", 
            fontface =  "italic"
        )

    if (add_jeco) {
        my_graph <- my_graph +
            annotate(
                geom = "text",
                label = "J\u00c9CO",
                x = structure(-Inf, class = "Date"), y = -Inf,
                hjust = -0.1, vjust = -0.5, col = "#CB0E62", 
                fontface =  "bold"
            )
    }

    my_graph <- my_graph +
        theme_bw() +
        theme(
            legend.title = element_text(face = "bold", colour = "#0D3648"),
            panel.background = element_rect(fill = "#F2E6DC", colour = NA),
            panel.border = element_rect(colour = NA),
            panel.grid.major = element_line(colour = "grey"),
            panel.grid.minor = element_line(colour = "grey"),
            axis.line = element_line(colour = NA),
            axis.text = element_text(colour = "#0D3648"),
            axis.ticks = element_line(colour = "#0D3648"),
            axis.title = element_text(colour = "#0D3648"),
            plot.background = element_rect(fill = "transparent", colour = NA),
            plot.title = element_text(face = "bold", colour = "#0D3648")
        )

    if (!is.null(filename)) {
        ggsave(file.path(directory, "svg", paste0(filename,".svg")), plot = my_graph, height = 7, width = 10)
        ggsave(file.path(directory, "png", paste0(filename,".png")), plot = my_graph, height = 7, width = 10)
    }
    
    return(my_graph)
}
