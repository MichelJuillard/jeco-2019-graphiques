# JECO-2019-graphics

The R program used to make the graphs for illustrating the conference "[La transition c'est maintenant](http://www.journeeseconomie.org/index.php?arc=a6&num=659)" at the Journées de l'Economie, in Lyon, 5-7 Novembre, 2019.

The program 
 - downloads data from [DBnomics](https://db.nomics.world) using the package [rdbnomics](https://cran.r-project.org/web/packages/rdbnomics/index.html)
 - prepares data with dplyr
 - draws the graphs with ggplot2
 - save the graphs in format PNG (directory "session_659/png") and in format SVG (directory "session_659/svg")
 
Files:
 - graphic_common.R: defines function jeco_graph() for line graphs and function jeco_bar_graph() for bar graphs
 - get_data.R: dowload data from DBnomics
 - make_graphs.R: selects the variables and draws the graphs

## Usage

```
# Load graphic functions
source("graphic_common.R")

# Download data from DBnomics
# This steps needs to be done only once
#   as the data are saved in emissions.RData 
source("get_data.R")

# Draw and save the graphs in sub-directory session_659
source("make_graphs.R")
```
